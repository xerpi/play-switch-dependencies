## A CMake helper to find glm on the system.
##
## On success, it will define:
## > GLM_FOUND        - The system has glm
## > GLM_INCLUDE_DIRS - The glm include directories
## > GLM_LIBRARIES    - The static glm libraries
##
## It also adds an imported target named `switch::glm`.
##
## ```
## target_link_libraries(my_executable switch::glm)
## ```
## is equivalent to
## ```
## target_include_directories(my_executable PRIVATE ${GLM_INCLUDE_DIRS})
## target_link_libraries(my_executable ${GLM_LIBRARIES})
## ```
##
## By default, CMake will look for glm in $DEVKITPRO/portlibs/switch.
## If you want to use a custom fork, you need to override the
## `LIBNX` variable within CMake before this file is used.

include(FindPackageHandleStandardArgs)

if(NOT SWITCH)
    message(FATAL_ERROR "This helper can only be used when cross-compiling for the Switch.")
endif()

set(GLM_PATHS ${GLM} $ENV{GLM} ${DEVKITPRO}/portlibs/switch portlibs/switch)

find_path(GLM_INCLUDE_DIR glm/glm.hpp
        PATHS ${GLM_PATHS}
        PATH_SUFFIXES include)

set(GLM_INCLUDE_DIRS ${GLM_INCLUDE_DIR})

find_package_handle_standard_args(GLM DEFAULT_MSG
        GLM_INCLUDE_DIR)

mark_as_advanced(GLM_INCLUDE_DIR)

if(GLM_FOUND)
    set(GLM ${GLM_INCLUDE_DIR}/..)

    add_library(switch::glm INTERFACE IMPORTED GLOBAL)
    set_target_properties(switch::glm PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${GLM_INCLUDE_DIR}")
endif()
